"""REST client handling, including FreshworksStream base class."""

import copy
import requests
from datetime import timedelta
from pathlib import Path
from pendulum import parse
from typing import Any, Dict, Optional, Iterable

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class FreshworksStream(RESTStream):
    """Freshworks stream class."""


    # OR use a dynamic url_base:
    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        return f"https://{self.config['org_name']}.myfreshworks.com/crm/sales/api"

    records_jsonpath = "$[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.next_page"  # Or override `get_next_page_token`.

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        # If not using an authenticator, you may also provide inline auth headers:
        headers["Authorization"] = f'Token token={self.config.get("auth_token")}'
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        if self.replication_key:
            params["sort"] = self.replication_key
            params["sort_type"] ="asc"
        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        # TODO: Parse response body and return a set of records.
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def get_url(self, context: Optional[dict]) -> str:
        url = "".join([self.url_base, self.path or ""])
        vals = copy.copy(dict(self.config))
        vals.update(context or {})
        for k, v in vals.items():
            search_text = "".join(["{", k, "}"])
            if search_text in url:
                url = url.replace(search_text, self._url_encode(v))

        # if self.tap_stream_id=='contacts':
        #     url = f'{url}{self.config.get("contact_view")}'

        # if self.tap_stream_id=='accounts':
        #     url = f'{url}{self.config.get("account_view")}'

        # if self.tap_stream_id=='deals':
        #     url = f'{url}{self.config.get("deal_view")}'

        return url

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        start_time = rep_key or start_date
        start_time = start_time + timedelta(seconds=1)
        return start_time

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects."""
        if self.replication_key:
            for record in super().get_records(context):
                if self.get_starting_time(context) <= parse(record.get(self.replication_key)):
                    yield record
        else:
            yield from super().get_records(context)