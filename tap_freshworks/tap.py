"""Freshworks tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_freshworks.streams import (
    FreshworksStream,
    ContactFilterStream,
    AccountFilterStream,
    DealFilterStream,
    ContactsStream,
    ContactActivityStream,
    AccountsStream,
    AppointmentsStream,
    DealsStream,


)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    ContactFilterStream,
    AccountFilterStream,
    DealFilterStream,
    ContactsStream,
    ContactActivityStream,
    AccountsStream,
    AppointmentsStream,
    DealsStream
]


class TapFreshworks(Tap):
    """Freshworks tap class."""
    name = "tap-freshworks"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "org_name",
            th.StringType,
            required=True
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            required=False,
            description="The start date to use as fallback date if state is not present"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapFreshworks.cli()
