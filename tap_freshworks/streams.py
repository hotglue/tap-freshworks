from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th

from tap_freshworks.client import FreshworksStream


class ContactFilterStream(FreshworksStream):
    name = "contactfilters"
    path = "/contacts/filters"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.filters[*]"

    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("name",th.StringType),
        th.Property("model_class_name",th.StringType),
        th.Property("user_id",th.NumberType),
        th.Property("is_default",th.BooleanType),
        th.Property("updated_at",th.DateTimeType),
        th.Property("user_name",th.StringType),
        th.Property("current_user_permissions",th.ArrayType(th.StringType))
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"view_id": record["id"]}

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        for record in super().get_records(context):
            if record['name'] == 'All Contacts':
                yield record
            else:
                continue


class ContactsStream(FreshworksStream):
    name = "contacts"
    path = "/contacts/view/{view_id}"
    primary_keys = ["id"]
    replication_key = "updated_at"
    records_jsonpath = "$.contacts[*]"
    parent_stream_type = ContactFilterStream
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("display_name", th.StringType),
        th.Property("avatar", th.StringType),
        th.Property("job_title", th.StringType),
        th.Property("city", th.StringType),
        th.Property("state", th.StringType),
        th.Property("zipcode", th.StringType),
        th.Property("country", th.StringType),
        th.Property("email", th.StringType),
        th.Property("emails", th.ArrayType(
            th.ObjectType(
                th.Property('id',th.NumberType),
                th.Property('value',th.StringType),
                th.Property('is_primary',th.BooleanType),
                th.Property('label',th.StringType),
                th.Property('_destroy',th.BooleanType),
            )
        )),
        th.Property("time_zone", th.StringType),
        th.Property("work_number", th.StringType),
        th.Property("mobile_number", th.StringType),
        th.Property("address", th.StringType),
        th.Property("last_seen", th.StringType),
        th.Property("lead_score", th.NumberType),
        th.Property("last_contacted", th.DateTimeType),
        th.Property("open_deals_amount", th.StringType),
        th.Property("won_deals_amount", th.StringType),
        th.Property("links", th.ObjectType(
            th.Property('conversations',th.StringType),
            th.Property('timeline_feeds',th.StringType),
            th.Property('document_associations',th.StringType),
            th.Property('notes',th.StringType),
            th.Property('tasks',th.StringType),
            th.Property('appointments',th.StringType),
            th.Property('reminders',th.StringType),
            th.Property('duplicates',th.StringType),
            th.Property('connections',th.StringType),
        )),
        th.Property("last_contacted_sales_activity_mode", th.StringType),
        th.Property("custom_field", th.CustomType({"type": ["object"]})),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("keyword", th.StringType),
        th.Property("medium", th.StringType),
        th.Property("last_contacted_mode", th.StringType),
        th.Property("recent_note", th.StringType),
        th.Property("won_deals_count", th.NumberType),
        th.Property("last_contacted_via_sales_activity", th.DateTimeType),
        th.Property("completed_sales_sequences", th.StringType),
        th.Property("active_sales_sequences", th.StringType),
        th.Property("web_form_ids", th.StringType),
        th.Property("open_deals_count", th.NumberType),
        th.Property("last_assigned_at", th.DateTimeType),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("facebook", th.StringType),
        th.Property("twitter", th.StringType),
        th.Property("linkedin", th.StringType),
        th.Property("is_deleted", th.BooleanType),
        th.Property("team_user_ids", th.ArrayType(th.NumberType)),
        th.Property("external_id", th.StringType),
        th.Property("work_email", th.StringType),
        th.Property("subscription_status", th.NumberType),
        th.Property("subscription_types", th.StringType),
        th.Property("customer_fit", th.NumberType),
        th.Property("whatsapp_subscription_status", th.NumberType),
        th.Property("phone_numbers", th.ArrayType(th.StringType)),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"id": record["id"]}


class ContactActivityStream(FreshworksStream):
    name = "contactactivities"
    path = "/contacts/{id}/activities"
    primary_keys = ["id"]
    replication_key = "created_at"
    records_jsonpath = "$.activities[*]"
    parent_stream_type = ContactsStream
    schema = th.PropertiesList(
        th.Property('id',th.StringType),
        th.Property('created_at',th.DateTimeType),
        th.Property('automation',th.StringType),
        th.Property('action_data',th.ObjectType(
            th.Property('outcome',th.StringType),
            th.Property('recording_duration',th.NumberType),
            th.Property('direction',th.StringType),
            th.Property('phone_caller',th.ObjectType(
                th.Property('phone_caller',th.ObjectType(
                    th.Property('number',th.NumberType)
                ))
            )),
        )),
        th.Property('additional_timeline_info',th.ObjectType(
            th.Property('phone_calls',
                th.ArrayType(
                    th.ObjectType(
                        th.Property('id',th.NumberType),
                        th.Property('account_id',th.NumberType),
                        th.Property('call_cost',th.NumberType),
                        th.Property('call_direction',th.BooleanType),
                        th.Property('call_duration',th.NumberType),
                        th.Property('call_flow',th.StringType),
                        th.Property('call_sid',th.StringType),
                        th.Property('call_status',th.NumberType),
                        th.Property('created_at',th.DateTimeType),
                        th.Property('entity_id',th.NumberType),
                        th.Property('entity_type',th.StringType),
                        th.Property('freshcaller_agent_leg_id',th.NumberType),
                        th.Property('freshcaller_id',th.StringType),
                        th.Property('freshcaller_number',th.StringType),
                        th.Property('freshcaller_number_country',th.StringType),
                        th.Property('is_deleted',th.BooleanType),
                        th.Property('is_manual',th.BooleanType),
                        th.Property('needs_response',th.BooleanType),
                        th.Property('outcome_id',th.NumberType),
                        th.Property('phone_account_id',th.NumberType),
                        th.Property('phone_caller_id',th.NumberType),
                        th.Property('phone_number_id',th.NumberType),
                        th.Property('recording_duration',th.NumberType),
                        th.Property('recording_url',th.StringType),
                        th.Property('root_phone_call_id',th.NumberType),
                        th.Property('source',th.StringType),
                        th.Property('updated_at',th.DateTimeType),
                        th.Property('user_id',th.NumberType)
                    )
                ),
                th.Property('name',th.ObjectType(
                    th.Property('display_name',th.StringType)
                ))
            )
        )),
        th.Property('group',th.ObjectType(
            th.Property('id',th.StringType),
            th.Property('name',th.StringType),
            th.Property('type',th.StringType),
        )),
        th.Property('targetable_id',th.NumberType),
        th.Property('targetable_name',th.StringType),
        th.Property('targetable_type',th.StringType),
        th.Property('composite_id',th.StringType),
        th.Property('action_type',th.StringType),
        th.Property('user_activity',th.BooleanType),
        th.Property('actionable_id',th.NumberType),
        th.Property('actionable_type',th.StringType),
    ).to_dict()


class AccountFilterStream(FreshworksStream):
    name = "accountfilters"
    path = "/sales_accounts/filters"
    primary_keys = ["id"]
    records_jsonpath = "$.filters[*]"
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("name",th.StringType),
        th.Property("model_class_name",th.StringType),
        th.Property("user_id",th.NumberType),
        th.Property("is_default",th.BooleanType),
        th.Property("is_public",th.BooleanType),
        th.Property("updated_at",th.DateTimeType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"view_id": record["id"]}

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        for record in super().get_records(context):
            if record['name'] == 'All Accounts':
                yield record
            else:
                continue


class AccountsStream(FreshworksStream):
    name = "accounts"
    path = "/sales_accounts/view/{view_id}"
    parent_stream_type = AccountFilterStream
    primary_keys = ["id"]
    replication_key = "updated_at"
    records_jsonpath = "$.sales_accounts[*]"
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("name", th.StringType),
        th.Property("address", th.StringType),
        th.Property("city", th.StringType),
        th.Property("state", th.StringType),
        th.Property("zipcode", th.StringType),
        th.Property("country", th.StringType),
        th.Property("number_of_employees", th.NumberType),
        th.Property("annual_revenue", th.NumberType),
        th.Property("website", th.StringType),
        th.Property("owner_id", th.NumberType),
        th.Property("phone", th.StringType),
        th.Property("open_deals_amount", th.StringType),
        th.Property("won_deals_amount", th.StringType),
        th.Property("won_deals_count", th.NumberType),
        th.Property("last_contacted", th.DateTimeType),
        th.Property("last_contacted_mode", th.StringType),
         th.Property("facebook", th.StringType),
        th.Property("twitter", th.StringType),
        th.Property("linkedin", th.StringType),
        th.Property("links", th.ObjectType(
            th.Property('conversations',th.StringType),
            th.Property('document_associations',th.StringType),
            th.Property('notes',th.StringType),
            th.Property('tasks',th.StringType),
            th.Property('appointments',th.StringType)
        )),
        th.Property("custom_field", th.CustomType({"type": ["object"]})),
        th.Property("created_at", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("avatar", th.StringType),
        th.Property("parent_sales_account_id", th.NumberType),
        th.Property("recent_note", th.NumberType),
        th.Property("last_contacted_via_sales_activity", th.DateTimeType),
        th.Property("last_contacted_sales_activity_mode", th.StringType),
        th.Property("active_sales_sequences", th.NumberType),
        th.Property("last_assigned_at", th.DateTimeType),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("is_deleted", th.BooleanType),
        th.Property("team_user_ids", th.ArrayType(th.NumberType)),
    ).to_dict()


class DealFilterStream(FreshworksStream):
    name = "dealsfilters"
    path = "/deals/filters"
    primary_keys = ["id"]
    records_jsonpath = "$.filters[*]"
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("name",th.StringType),
        th.Property("model_class_name",th.StringType),
        th.Property("user_id",th.NumberType),
        th.Property("is_default",th.BooleanType),
        th.Property("is_public",th.BooleanType),
        th.Property("updated_at",th.DateTimeType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"view_id": record["id"]}

    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        for record in super().get_records(context):
            if record['name'] == 'My Deals':
                yield record
            else:
                continue


class AppointmentsStream(FreshworksStream):
    name = "appointments"
    path = "/appointments"
    primary_keys = ["id"]
    replication_key = "updated_at"
    records_jsonpath = "$.appointments[*]"
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("time_zone",th.StringType),
        th.Property("title",th.StringType),
        th.Property("description",th.StringType),
        th.Property("location",th.StringType),
        th.Property("is_allday",th.BooleanType),
        th.Property("outcome_id",th.NumberType),
        th.Property("from_date",th.DateTimeType),
        th.Property("end_date",th.DateTimeType),
        th.Property("created_at",th.DateTimeType),
        th.Property("updated_at",th.DateTimeType),
        th.Property("provider",th.StringType),
        th.Property("creater_id",th.NumberType),
        th.Property("latitude",th.NumberType),
        th.Property("longitude",th.NumberType),
        th.Property("longitude",th.NumberType),
        th.Property("checkedin_at",th.StringType),
        th.Property("can_checkin_checkout",th.BooleanType),
        th.Property("can_checkin_checkout",th.BooleanType),
        th.Property("checkedout_latitude",th.NumberType),
        th.Property("checkedout_longitude",th.NumberType),
        th.Property("checkedout_longitude",th.NumberType),
        th.Property("checkedout_location",th.StringType),
        th.Property("checkedout_at",th.StringType),
        th.Property("checkedin_duration",th.StringType),
        th.Property("can_checkin",th.BooleanType),
        th.Property("conference_id",th.NumberType),
    ).to_dict()


class DealsStream(FreshworksStream):
    name = "deals"
    path = "/deals/view/{view_id}"
    primary_keys = ["id"]
    replication_key = "updated_at"
    records_jsonpath = "$.deals[*]"
    parent_stream_type = DealFilterStream
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("name",th.StringType),
        th.Property("amount",th.StringType),
        th.Property("base_currency_amount",th.StringType),
        th.Property("expected_close",th.StringType),
        th.Property("closed_date",th.BooleanType),
        th.Property("stage_updated_time",th.DateTimeType),
        th.Property("custom_field", th.CustomType({"type": ["object"]})),
        th.Property("probability", th.NumberType),
        th.Property("updated_at", th.DateTimeType),
        th.Property("created_at", th.DateTimeType),
        th.Property("deal_pipeline_id", th.NumberType),
        th.Property("deal_stage_id", th.NumberType),
        th.Property("age", th.NumberType),
        th.Property("links", th.ObjectType(
            th.Property('conversations',th.StringType),
            th.Property('document_associations',th.StringType),
            th.Property('notes',th.StringType),
            th.Property('tasks',th.StringType),
            th.Property('appointments',th.StringType)
        )),
        th.Property("recent_note", th.StringType),
        th.Property("completed_sales_sequences", th.StringType),
        th.Property("active_sales_sequences", th.StringType),
        th.Property("web_form_id", th.StringType),
        th.Property("upcoming_activities_time", th.StringType),
        th.Property("collaboration", th.CustomType({"type": ["object"]})),
        th.Property("last_assigned_at", th.DateTimeType),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property("last_contacted_sales_activity_mode", th.StringType),
        th.Property("last_contacted_via_sales_activity", th.DateTimeType),
        th.Property("expected_deal_value", th.StringType),
        th.Property("is_deleted", th.BooleanType),
        th.Property("team_user_ids", th.ArrayType(th.NumberType)),
        th.Property("avatar", th.StringType),
        th.Property("fc_widget_collaboration", th.ObjectType(
            th.Property('convo_token',th.StringType),
            th.Property('auth_token',th.StringType),
            th.Property('encoded_jwt_token',th.StringType)
        )),
        th.Property("forecast_category", th.StringType),
        th.Property("deal_prediction", th.NumberType),
        th.Property("deal_prediction_last_updated_at", th.DateTimeType),
        th.Property("last_deal_prediction", th.StringType),
        th.Property("rotten_days", th.StringType),
        th.Property("has_products", th.BooleanType),
        th.Property("products", th.StringType),
    ).to_dict()